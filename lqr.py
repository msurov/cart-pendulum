import numpy as np
from scipy.linalg import solve_continuous_are
from scipy.interpolate import make_interp_spline
from integrate import integrate


def lqr_lti(A, B, Q, R):
    R'''
        :param A, B: linear system matrices of dimensions nxn, and nxm \
        :param Q, R: weighted matrices of dimensions nxn, mxm \
        :result: K,P
    '''
    P = solve_continuous_are(A, B, Q, R)
    K = -np.linalg.inv(R) @ (B.T @ P)
    return K, P


def lqr_ltv(t, A, B, Q, R, S):
    R'''
        :param t: time knots of length K \
        :param A: array of dim KxNxN, values of matrix A at knots t \
        :param B: array of dim KxNxM, values of matrix B at knots t \
        :param Q: array of dim KxNxN, x weight coefficients \
        :param R: array of dim KxMxM, u weight coefficients \
        :param S: array of dim NxN, x weight coefficients \
        :result:
            K array of dim KxMxN, controller coefficients
            P array of dim KxNxN, solution of the Riccati equation
        Find control of the form u = K x that minimizes the functional
        \[
            J=\int_{0}^{T}u_{s}^{T}R_{s}u_{s}+x_{s}^{T}Q_{s}x_{s}ds+x_{T}^{T}Sx_{T}
        \]
    '''
    N,n,m = B.shape
    assert A.shape == (N,n,n)
    assert Q.shape == (N,n,n)
    assert R.shape == (N,m,m)
    assert S.shape == (n,n)

    t0, t1 = t[0], t[-1]
    step = (t1 - t0) / (len(t) - 1)

    assert np.allclose(np.diff(t), step), 'Expect \'t\' has uniform step'

    fun_A = make_interp_spline(t, A, k=3)
    fun_B = make_interp_spline(t, B, k=3)
    fun_Q = make_interp_spline(t, Q, k=3)
    inv_R = np.array([np.linalg.inv(R[i,:,:]) for i in range(len(t))])
    fun_inv_R = make_interp_spline(t, inv_R, k=3)

    def rhs_P(t, P):
        A_ = fun_A(t)
        B_ = fun_B(t)
        Q_ = fun_Q(t)
        Rinv_ = fun_inv_R(t)
        return -A_.T @ P - P @ A_ + P @ B_ @ Rinv_ @ B_.T @ P - Q_

    t_, P_ = integrate(rhs_P, S, [t1, t0], step=step)
    P = P_[::-1]
    K = np.zeros((len(t), m, n))

    for i in range(len(t)):
        K[i,:,:] = -inv_R[i,:,:] @ B[i,:,:].T @ P[i,:,:]

    return K, P
