import os.path
import numpy as np
import scipy.io



def save(filename, data):
    if filename is None:
        return None

    _,ext = os.path.splitext(filename)
    if ext == '.npy':
        np.save(filename, data)
    elif ext == '.npz':
        np.savez_compressed(filename, default=data)
    elif ext == '.txt':
        np.savetxt(filename, data)
    elif ext == '.mat':
        scipy.io.savemat(filename, data)
    else:
        return None

    return 'ok'


def load(filename):
    if filename is None:
        return None

    if not os.path.isfile(filename):
        return None

    _,ext = os.path.splitext(filename)
    if ext == '.npy':
        return np.load(filename, allow_pickle=True)
    elif ext == '.txt':
        return np.loadtxt(filename)
    elif ext == '.mat':
        return scipy.io.loadmat(filename)

    return None


def load_dict(filename):
    if filename is None:
        return None

    if not os.path.isfile(filename):
        return None

    _,ext = os.path.splitext(filename)
    if ext == '.npy':
        return np.load(filename, allow_pickle=True).item()
    elif ext == '.npz':
        d = np.load(filename)
        return d['default'].item()
    elif ext == '.mat':
        return scipy.io.loadmat(filename)

    return None

