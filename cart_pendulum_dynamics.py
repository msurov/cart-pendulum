import sympy as sy
import numpy as np
from collections import namedtuple



def maketuple(name : str, d : dict):
    T = namedtuple(name, sorted(d))
    return T(**d)


class Dynamics:
    def __init__(self):
        s,θ = sy.symbols(R's \theta', real=True)
        ds,dθ = sy.symbols(R'\dot{s} \dot{\theta}', real=True)
        q = [s,θ]
        dq = [ds,dθ]

        J,m,L,g,m_cart = sy.symbols(R'J m L g m_{cart}', real=True, positive=True)

        p1 = J + m*L**2
        p2 = m*L
        p3 = m*g*L
        p4 = m + m_cart

        parameters = {
            J: 0.02,
            m: 0.1,
            m_cart: 0.1,
            g: 9.8,
            L: 0.5,
        }
        cos = sy.cos
        sin = sy.sin

        M = sy.Matrix([
            [p4, p2*cos(θ)],
            [p2*cos(θ), p1],
        ])
        C = sy.Matrix([
            [0, -p2*sin(θ)*dθ],
            [0, 0]
        ])
        G = sy.Matrix([
            [0],
            [-p3*sin(θ)]
        ])
        B = sy.Matrix([
            [1],
            [0]
        ])
        Bp = sy.Matrix([
            [0, 1]
        ])

        f34 = M.inv() * (-C * sy.Matrix([[ds], [dθ]]) - G)
        f = sy.Matrix([[ds], [dθ], [f34[0]], [f34[1]]])
        g34 = M.inv() * B
        g = sy.Matrix([[0], [0], [g34[0]], [g34[1]]])
        Jf = f.jacobian([s,θ,ds,dθ])
        Jg = g.jacobian([s,θ,ds,dθ])
        g_perp = sy.Matrix([[0,0,0,1]])

        self.parameters = parameters
        self.expr = maketuple('Expr', {
            'q': [s,θ],
            'dq': [ds,dθ],
            'M': M,
            'C': C,
            'G': G,
            'B': B,
            'Bp': Bp,
            'f': f,
            'g': g,
            'Jf': Jf,
            'Jg': Jg,
            'p1': p1,
            'p2': p2,
            'p3': p3,
            'p4': p4,
        })
        self.fun = maketuple('Fun', {
            'M': sy.Lambda(q, M),
            'C': sy.Lambda(q + dq, C),
            'G': sy.Lambda(q, G),
            'B': sy.Lambda(q, B),
            'Bp': sy.Lambda(q, Bp),
        })
        self.num = maketuple('Num', {
            'M': self.makenum(q, M),
            'C': self.makenum(q + dq, C),
            'G': self.makenum(q, G),
            'B': self.makenum(q, B),
            'Bp': self.makenum(q, Bp),
            'rhs': self.num_rhs,
            'f': self.makenum([s,θ,ds,dθ], f),
            'g': self.makenum([s,θ,ds,dθ], g),
            'Jf': self.makenum([s,θ,ds,dθ], Jf),
            'Jg': self.makenum([s,θ,ds,dθ], Jg),
        })

    def makenum(self, args, expr):
        return sy.lambdify(args, expr.subs(self.parameters), 'numpy')

    def num_rhs(self, q, dq, u):
        M = self.num.M(*q)
        C = self.num.C(*q, *dq)
        G = self.num.G(*q)
        B = self.num.B(*q)
        dq = np.reshape(dq, (-1,1))
        ddq = np.linalg.inv(M) @ (- C @ dq - G + B * u)
        ddq = np.reshape(ddq, (-1,))
        return ddq


dynamics = Dynamics()


def test():
    d = dynamics
    x = np.random.random(4) - 0.5
    v = np.random.random(4) - 0.5
    ε = 1e-7
    fv1 = (d.num.f(*(x + ε * v)) - d.num.f(*x)) / ε
    fv1 = np.reshape(fv1, (-1,))
    fv2 = d.num.Jf(*x) @ v

    gv1 = (d.num.g(*(x + ε * v)) - d.num.g(*x)) / ε
    gv1 = np.reshape(gv1, (-1,))
    gv2 = d.num.Jg(*x) @ v

    assert np.linalg.norm(fv1 - fv2) < 1e-5, 'Jacobian of f is incorrect'
    assert np.linalg.norm(gv1 - gv2) < 1e-5, 'Jacobian of g is incorrect'


if __name__ == '__main__':
    test()
