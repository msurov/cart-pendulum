from integrate import integrate, integrate_fixedstep
from serialize import save, load_dict

import numpy as np
from scipy.optimize import fsolve, brentq
import sympy as sy
from cart_pendulum_dynamics import dynamics
import matplotlib.pyplot as plt
import matplotlib
from matplotlib import rc
from scipy.interpolate import make_interp_spline

from transverse_linearization import NonlinSystem, evf_linearized, phase_to_transverse
from lqr import lqr_ltv
import animate



matplotlib.rcParams['font.size'] = 14
matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42
matplotlib.rcParams['text.usetex'] = True
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
rc('text', usetex=True)


def integrate_dynamics(tspan, q0, dq0, u, step=1e-3):
    def f(t, state):
        q = state[0:2]
        dq = state[2:4]
        ddq = dynamics.num.rhs(q, dq, u(t, q, dq))
        return np.concatenate((dq, ddq))

    state0 = np.concatenate((
        np.reshape(q0, (-1,)),
        np.reshape(dq0, (-1,))
    ))
    t, state = integrate(f, state0, tspan, step=step)
    return t, state


def get_ddx_num():
    p1 = dynamics.expr.p1
    p2 = dynamics.expr.p2
    p3 = dynamics.expr.p3
    θ = sy.Dummy()
    ddθ = sy.Dummy()
    expr = (p3 * sy.sin(θ) - p1 * ddθ) / (p2 * sy.cos(θ))
    return dynamics.makenum([θ,ddθ], expr)


def get_feedforward_num():
    '''
        Feed-forward as function of θstar, dot{θstar}, ddot{θstar}
    '''
    p1 = dynamics.expr.p1
    p2 = dynamics.expr.p2
    p3 = dynamics.expr.p3
    p4 = dynamics.expr.p4
    θ = sy.Dummy()
    dθ = sy.Dummy()
    ddθ = sy.Dummy()
    
    expr = p4*p3*sy.tan(θ) / p2 \
        + (p2**2 * sy.cos(θ)**2 - p4*p1) * ddθ / (p2 * sy.cos(θ)) \
        - p2 * sy.sin(θ) * dθ**2

    return dynamics.makenum([θ,dθ,ddθ], expr)


def find_trajectory():
    T = 2.
    t = np.linspace(0, T, 200)
    θnum = make_interp_spline(t,
        0.5 * np.sin(2*np.pi*t/T) + 0.3 * np.sin(3*2*np.pi*t/T + 1.)
    )
    θnum.extrapolate = 'periodic'
    ddxnum = get_ddx_num()
    tmp = lambda t,state: [state[1], ddxnum(θnum(t), θnum(t, 2))]
    t, state = integrate(tmp, [0, 0], [0,T], step=1e-3)
    ds0 = -state[-1,0] / T

    fnum = get_feedforward_num()
    feedforward = lambda t,q,dq: fnum(θnum(t), θnum(t,1), θnum(t,2))
    q0 = [0, θnum(0)]
    dq0 = [ds0, θnum(0, 1)]
    t, state = integrate_dynamics([0, T], q0, dq0, feedforward, step=1e-3)
    u = np.array([feedforward(ti,None,None) for ti in t])

    traj = {
        't': t,
        'u': u,
        'x': state[:,0],
        'dx': state[:,2],
        'phi': state[:,1],
        'dphi': state[:,3]
    }
    save('data/traj1/traj.npz', traj)

    fig,axes = plt.subplots(2, 2)
    ax = axes[0,0]
    ax.plot(state[:,1], state[:,0])
    ax.set_xlabel(R'$\phi$')
    ax.set_ylabel(R'$x$')
    ax.grid()

    ax = axes[0,1]
    ax.plot(state[:,0], state[:,2])
    ax.set_xlabel(R'$x$')
    ax.set_ylabel(R'$\dot{x}$')
    ax.grid()

    ax = axes[1,0]
    ax.plot(state[:,1], state[:,3])
    ax.set_xlabel(R'$\phi$')
    ax.set_ylabel(R'$\dot{\phi}$')
    ax.grid()

    ax = axes[1,1]
    ax.plot(t, u)
    ax.set_xlabel(R'$t$')
    ax.set_ylabel(R'$u$')
    ax.grid()

    plt.savefig('data/traj1/traj.png')


def get_nonlin_sys(traj):
    _t = traj['t']
    _x = np.array([
        traj['x'],
        traj['phi'],
        traj['dx'],
        traj['dphi'],
    ]).T
    _u = traj['u']
    x = make_interp_spline(_t, _x)
    u = make_interp_spline(_t, np.reshape(_u, (-1,1)))
    nonlin = NonlinSystem(
        f = lambda x: np.reshape(dynamics.num.f(*x), (-1,)),
        g = lambda x: np.reshape(dynamics.num.g(*x), (4,1)),
        Jf = lambda x: dynamics.num.Jf(*x),
        Jg = lambda x: np.reshape(dynamics.num.Jg(*x), (4,1,4)),
        P = np.eye(4),
        g_perp = None,
        x_star = x,
        u_star = u
    )
    return nonlin


def transverse_linearization():
    traj = load_dict('data/traj1/traj.npz')
    _t = traj['t']
    nonlin = get_nonlin_sys(traj)
    _A = np.zeros((len(_t), 3, 3))
    _B = np.zeros((len(_t), 3, 1))
    for i in range(len(_t)):
        A,B = evf_linearized(nonlin, _t[i])
        _A[i,:,:] = A
        _B[i,:,:] = B

    linsys = {
        'A': _A,
        'B': _B,
        't': _t,
    }

    save('data/traj1/linsys.npz', linsys)


def linsys_lqr():
    linsys = load_dict('data/traj1/linsys.npz')
    t = linsys['t']
    A = linsys['A']
    B = linsys['B']
    n = len(t)
    Q = np.array([np.diag([1.,1.,5.])] * n)
    R = np.array([0.0001*np.eye(1)] * n)
    S = 0.1 * Q[-1,:,:]
    K,P = lqr_ltv(t, A, B, Q, R, S)
    lqr = {
        't': t,
        'K': K,
        'P': P
    }
    save('data/traj1/lqr.npz', lqr)


def simulate():
    traj = load_dict('data/traj1/traj.npz')
    nonlin = get_nonlin_sys(traj)
    f = dynamics.num.f
    g = dynamics.num.g
    ctrl = load_dict('data/traj1/lqr.npz')
    K = make_interp_spline(ctrl['t'], ctrl['K'])
    feedforward = make_interp_spline(traj['t'], traj['u'])

    def linctr(state):
        τ,ξ = phase_to_transverse(nonlin, state)
        return K(τ) @ ξ

    def nonlinctrl(state):
        τ,ξ = phase_to_transverse(nonlin, state)
        return feedforward(τ) + K(τ) @ ξ

    def transverse(state):
        _,ξ = phase_to_transverse(nonlin, state)
        return ξ

    def rhs(t, state):
        τ,ξ = phase_to_transverse(nonlin, state)
        u = feedforward(τ) + K(τ) @ ξ
        return f(*state) + g(*state) * u

    _t = traj['t']
    T = _t[-1]
    np.random.seed(0)
    state0 = np.array([traj['x'][0], traj['phi'][0], traj['dx'][0], traj['dphi'][0]])
    state0 += np.random.normal(size=4, scale=0.1)
    t, state = integrate_fixedstep(rhs, state0, [0, 3*T], step=1e-3)

    traj = {
        't': t,
        'x': state[:,0],
        'dx': state[:,2],
        'phi': state[:,1],
        'dphi': state[:,3],
        'u': np.array([nonlinctrl(x) for x in state]),
        'v': np.array([linctr(x) for x in state]),
        'ξ': np.array([transverse(x) for x in state]),
    }

    save('data/traj1/trans.npz', traj)


def plots():
    traj = load_dict('data/traj1/traj.npz')
    trans = load_dict('data/traj1/trans.npz')

    fig,axes = plt.subplots(2,2)

    ax = axes[0,0]
    ax.plot(traj['phi'], traj['x'], '--', color='r', lw=2)
    ax.plot(trans['phi'], trans['x'], color='b', lw=0.5)
    ax.set_xlabel(R'$\phi$')
    ax.set_ylabel(R'$x$')
    ax.grid()

    ax = axes[0,1]
    ax.plot(traj['x'], traj['dx'], '--', color='r', lw=2)
    ax.plot(trans['x'], trans['dx'], color='b', lw=0.5)
    ax.set_xlabel(R'$x$')
    ax.set_ylabel(R'$\dot{x}$')
    ax.grid()

    ax = axes[1,0]
    ax.plot(traj['phi'], traj['dphi'], '--', color='r', lw=2)
    ax.plot(trans['phi'], trans['dphi'], color='b', lw=0.5)
    ax.set_xlabel(R'$\phi$')
    ax.set_ylabel(R'$\dot{\phi}$')
    ax.grid()

    ax = axes[1,1]
    ax.plot(trans['t'], trans['ξ'], lw=1.)
    ax.set_xlabel(R'$t$')
    ax.set_ylabel(R'$\xi$')
    ax.grid()

    plt.tight_layout()
    plt.show()


def anim_trajectory():
    traj = load_dict('data/traj1/traj.npz')
    animate.run(traj, saveto='data/traj1/traj.mp4', speed=0.5)


def anim_transient():
    traj = load_dict('data/traj1/traj.npz')
    trans = load_dict('data/traj1/trans.npz')
    animate.run(traj, trans, saveto='data/traj1/trans.mp4', speed=0.5)


if __name__ == '__main__':
    find_trajectory()
    anim_trajectory()
    transverse_linearization()
    linsys_lqr()
    simulate()
    anim_transient()
    plots()
