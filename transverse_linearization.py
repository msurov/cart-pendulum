from collections import namedtuple
import numpy as np
from scipy.optimize import root
import types
from integrate import integrate, integrate_periodic
from rotmat import pow
from scipy.interpolate import make_interp_spline
from scipy.optimize import fminbound


def evf_pi(sys, x):
    '''
        τ = π(x)
    '''
    P = sys.P
    t = np.linspace(sys.x_star.t[0], sys.x_star.t[-1], 100)
    x_star = sys.x_star

    d = x - x_star(t)
    d2 = np.sum(d.dot(P) * d, axis=1)
    i = np.argmin(d2)

    i1 = max(i - 1, 0)
    i2 = min(i + 1, len(d2) - 1)
    t1 = t[i1]
    t2 = t[i2]

    def cost_fun(t):
        d = x - x_star(t)
        d2 = d @ P @ d
        return d2

    t = fminbound(cost_fun, t1, t2, maxfun=10, disp=0, xtol=1e-8)
    return t


def evf_Jpi(sys, x):
    P = sys.P
    x_star = sys.x_star
    u_star = sts.u_star

    tau = evf_pi(sys, x)
    x_ = x_star(tau)
    h_ = sys.h_star(tau)
    Jh_ = sys.Jh_star(tau)
    n = h_.dot(P)
    d = h_.dot(P).dot(h_) - \
        h_.dot(Jh_.T).dot(P).dot(x - x_)
    return n / d


def evf_u_f(sys, x):
    R'''
        $u_{f}\left(x\right)=u_{\star}\left(\pi\left(x\right)\right)$
    '''
    t = evf_pi(sys, x)
    return sys.u_star(t)


def evf_h(sys, x):
    R'''
        $h\left(x\right)=f\left(x\right)+g\left(x\right)u_{f}\left(x\right)$
    '''
    P = sys.P
    f = sys.f(x)
    g = sts.g(x)
    u_f = evf_u_f(sys, x)
    return f + g.dot(u_f)


'''
    along trajectory x_star
'''

def evf_dot_u_star(sys, t):
    R'''
        $\dot{u}_{\star}\left(t\right)$
    '''
    return sys.u_star(t, 1)


def evf_Jh_star(sys, t):
    R'''
        $Jh_{\star}\left(t\right)=\left(\frac{\partial h}{\partial x}\right)_{x=x_{\star}\left(t\right)}=\left(\frac{\partial f\left(x\right)}{\partial x}\right)_{x_{\star}}+\left(\frac{\partial g\left(x\right)u_{\star}\left(t\right)}{\partial x}\right)_{x_{\star}}+g_{x_{\star}}\dot{u}_{\star}\frac{h_{\star}^{T}P}{h_{x_{\star}}^{T}Ph_{x_{\star}}}$
    '''
    x_ = sys.x_star(t)
    u_ = sys.u_star(t)
    dot_u_ = evf_dot_u_star(sys, t)

    Jf_ = sys.Jf(x_)
    Jg_ = sys.Jg(x_)
    f_ = sys.f(x_)
    g_ = sys.g(x_)
    h_ = f_ + g_.dot(u_)
    Ph_ = sys.P.dot(h_)
    Jh_ = Jf_ + np.tensordot(Jg_, np.reshape(u_, (-1,)), axes=(1,0)) + \
        np.outer(g_.dot(dot_u_), Ph_ / h_.dot(Ph_))
    return Jh_


def evf_h_star(sys, t):
    R'''
        $h_{\star}\left(t\right)=f_{\star}\left(t\right)+g_{\star}\left(t\right)u_{\star}\left(t\right)$
    '''
    x = sys.x_star(t)
    u = sys.u_star(t)
    return sys.f(x) + sys.g(x).dot(u)


def evf_perp(v):
    if len(v) == 2:
        P1 = np.array([[0., -1.], [1., 0.]])
        return np.array([P1.dot(v)]).T

    if len(v) == 4:
        P1 = np.array([
            # 0  1   2  3
            [ 0, 1,  0, 0],
            [-1, 0,  0, 0],
            [ 0, 0,  0, 1],
            [ 0, 0, -1, 0]
        ])
        P2 = np.array([
            # 0  1   2  3
            [ 0, 0,  1, 0],
            [ 0, 0,  0,-1],
            [-1, 0,  0, 0],
            [ 0, 1,  0, 0]
        ])
        P3 = np.array([
            # 0  1   2  3
            [ 0, 0,  0, 1],
            [ 0, 0,  1, 0],
            [ 0,-1,  0, 0],
            [-1, 0,  0, 0]
        ])
        return np.array([P1.dot(v), P2.dot(v), P3.dot(v)]).T

    assert False, 'Unsupported vector dimension'


def evf_E(sys, t):
    h_ = sys.h_star(t)
    Ph_ = sys.P.dot(h_)
    return evf_perp(Ph_ / np.linalg.norm(Ph_))


def evf_dot_E(sys, t):
    h_ = sys.h_star(t)
    Ph_ = sys.P.dot(h_)
    Jh_ = sys.Jh_star(t)
    n = len(h_)
    a = np.eye(n, n) - np.outer(Ph_, Ph_) / np.dot(Ph_, Ph_)
    b = sys.P.dot(Jh_).dot(h_) / np.linalg.norm(Ph_)
    return evf_perp(a.dot(b))


def evf_linearized(sys, t):
    R'''
        https://latex.codecogs.com/svg.latex?\bg_white&space;A\left(\tau\right):=E_{\tau}^{T}\left(\left(\frac{\partial&space;h}{\partial&space;x}\right)_{x_{\star}\left(\tau\right)}E_{\tau}-\frac{dE_{\tau}}{d\tau}\right)
    '''
    E = sys.E(t)
    dE = sys.dE(t)
    Jh_ = sys.Jh_star(t)
    x_ = sys.x_star(t)
    g_ = sys.g(x_)
    A = (dE.T + E.T.dot(Jh_)).dot(E)
    B = E.T.dot(g_)
    return A, B


def evf_linearized_v2(sys, t):
    R'''
        https://latex.codecogs.com/svg.latex?\bg_white&space;A&space;=&space;\left(E_{\tau}^{T}\frac{h_{x_{\star}}^{T}Ph_{x_{\star}}\left(\frac{\partial&space;h}{\partial&space;x}\right)_{x_{\star}}-h_{x_{\star}}h_{x_{\star}}^{T}P\left(\frac{\partial&space;h}{\partial&space;x}\right)_{x_{\star}}-h_{x_{\star}}h_{x_{\star}}^{T}\left(\frac{\partial&space;h}{\partial&space;x}\right)_{x_{\star}}^{T}P}{h_{x_{\star}}^{T}Ph_{x_{\star}}}&plus;\frac{dE_{\tau}^{T}}{d\tau}\right)E_{\tau}
    '''
    E = sys.E(t)
    dE = sys.dE(t)
    Jh_ = sys.Jh_star(t)
    h_ = sys.h_star(t)
    g_ = sys.g(sys.x_star(t))
    P = sys.P
    Ph_ = P.dot(h_)
    term1 = np.dot(h_.T.dot(Ph_), Jh_)
    term2 = -np.outer(h_, h_).dot(P).dot(Jh_)
    term3 = -np.outer(h_, h_).dot(Jh_.T).dot(P)

    num = term1 + term2 + term3
    den = h_.dot(Ph_)
    A = (E.T.dot(num / den) + dE.T).dot(E)
    I = np.eye(len(h_))
    B = E.T @ (I - np.outer(h_, h_) @ P / h_.dot(Ph_)) @ g_
    return A, B


def get_linearized(sys):
    assert False


def transverse_to_phase(sys, t, xi):
    n = len(t)
    xi = np.reshape(xi, (n, -1))
    x = sys.x_star(t) + \
        np.array([np.dot(sys.E(t_i), xi_i) for t_i,xi_i in zip(t, xi)])
    return x


def phase_to_transverse(sys, x):
    tau = evf_pi(sys, x)
    x_ = sys.x_star(tau)
    # if tau > sys.x_star.args[-1]:
    #     tau -= sys.x_star.args[-1]
    E = sys.E(tau)
    xi = E.T.dot(x - x_)
    return tau, xi


def __verify_shape(var, name, shape):
    assert var.shape == shape, 'g is expected to be %s' % str(shape)


def __verify_sys(sys):
    t = sys.x_star.t[sys.x_star.k + 1]
    x = sys.x_star(t)
    n = len(x)

    if sys.u_star is not None:
        u = sys.u_star(t)
        k = len(u)
        __verify_shape(sys.g(x), 'g', (n,k))

    if sys.g_perp is not None:
        __verify_shape(sys.g_perp(x), 'g_perp', (n,))
    __verify_shape(sys.Jh_star(t), 'Jh', (n,n))
    __verify_shape(sys.h_star(t), 'h', (n,))
    __verify_shape(sys.P, 'P', (n,n))


class System:
    f = None
    Jf = None
    g = None
    Jg = None
    g = None
    g_perp = None
    x_star = None
    u_star = None
    uf = None
    Juf = None
    h = None
    Jh = None
    h_star = None
    Jh_star = None


def exterior(a,b):
    return np.outer(a,b) - np.outer(b,a)


def normed(a):
    return a / np.linalg.norm(a)


def construct_basis(sys):
    print('[debug] constructing basis')
    P = sys.P
    v0 = normed(P @ sys.x_star(0,1))

    dim = len(v0)
    _,_,R0 = np.linalg.svd([v0])
    if (v0 @ R0)[0] < 0:
        R0 = -R0
    if np.linalg.det(R0) < 0:
        R0[:,-1] *= -1

    def rhs(t, R):
        v = P @ sys.x_star(t, 1)
        vn = v / np.linalg.norm(v)
        Dv = P @ sys.x_star(t, 2)
        Dvn = (np.eye(dim) - np.outer(vn,vn)) @ Dv / np.linalg.norm(v)
        return exterior(Dvn, vn) @ R

    T = sys.x_star.args[-1]
    tspan = [0, T]
    t,R = integrate(rhs, R0, tspan, step=1e-3)
    E = R[:,:,1:]
    evf_E = make_interp_spline(t, E, k=5)
    assert np.allclose([evf_E(ti).T @ evf_E(ti) for ti in t], np.eye(dim-1))
    return evf_E


def construct_periodic_basis(sys):
    print('[debug] constructing periodic basis')
    P = sys.P
    v0 = normed(P @ sys.x_star(0,1))

    dim = len(v0)
    _,_,R0 = np.linalg.svd([v0])
    if (v0 @ R0)[0] < 0:
        R0 = -R0
    if np.linalg.det(R0) < 0:
        R0[:,-1] *= -1

    def rhs(t, R):
        v = P @ sys.x_star(t, 1)
        vn = v / np.linalg.norm(v)
        Dv = P @ sys.x_star(t, 2)
        Dvn = (np.eye(dim) - np.outer(vn,vn)) @ Dv / np.linalg.norm(v)
        return exterior(Dvn, vn) @ R

    T = sys.x_star.period
    tspan = [0, T]
    t,R = integrate(rhs, R0, tspan, step=1e-3)

    # R = [project_SO(Ri) for Ri in R]
    # print(pow(R[-1].T @ R[0], 0.1 / T))
    assert np.allclose([Ri.T @ Ri for Ri in R], np.eye(dim))

    F = np.array([Ri @ pow(R[-1].T @ R[0], ti / T) for Ri,ti in zip(R,t)])
    E = F[:,:,1:]
    evf_E = make_interp_spline(t, E, k=5)
    evf_E.extrapolate = 'periodic'

    assert np.allclose([evf_E(ti).T @ evf_E(ti) for ti in t], np.eye(dim-1))
    assert np.allclose(evf_E(0), evf_E(T))
    # print([sys.x_star(ti, 1) @ P.T @ evf_E(ti) for ti in t])
    # assert np.allclose([sys.x_star(ti, 1) @ P.T @ evf_E(ti) for ti in t], 0, atol=1e-5)

    return evf_E


def NonlinSystem_v1(f, Jf, g, Jg, g_perp, x_star, u_star, P):
    R'''
        dx/dt = f(x) + g(x) u

        :param Jf: = ∂f/∂x
        :param Jg: = ∂g/∂x
        :param x_star, u_star: particular solution dx⋆(t)/dt = f(x⋆(t)) + g(x⋆(t)) u⋆(t)
        :param g_perp: g⊥ ⋅ g = I
        :param P: projection matrix
        :return: Nonlinear System class object 
    '''
    sys = System()
    sys.f = f
    sys.Jf = Jf
    sys.g = g
    sys.Jg = Jg
    sys.g_perp = g_perp
    sys.x_star = x_star
    sys.u_star = u_star
    sys.P = P
    sys.h_star = types.MethodType(evf_h_star, sys)
    sys.Jh_star = types.MethodType(evf_Jh_star, sys)

    T = sys.x_star.t[-1]
    # if np.allclose(sys.x_star(0,1), sys.x_star(T,1)):
    #     sys.E = construct_periodic_basis(sys)
    # else:
    #     sys.E = construct_basis(sys)

    sys.E = lambda t: evf_E(sys, t)
    sys.dE = lambda t: evf_dot_E(sys, t)

    __verify_sys(sys)
    return sys


def NonlinSystem_v2(h, Jh, g, g_perp, x_star, P):
    R'''
        dx/dt = h(x) + g(x) v
        :param Jh: = ∂h/∂x
        :param x_star: particular solution dx⋆(t)/dt = h(x⋆(t))
        :param g_perp: g⊥ ⋅ g = I
        :param P: projection matrix
        :return: Nonlinear System class object 
    '''
    sys = System()
    sys.h = h
    sys.Jh = Jh
    sys.g = g
    sys.g_perp = g_perp
    sys.x_star = x_star
    sys.P = P
    h_star = lambda s,t: s.h(s.x_star(t))
    Jh_star = lambda s,t: s.Jh(s.x_star(t))
    sys.h_star = types.MethodType(h_star, sys)
    sys.Jh_star = types.MethodType(Jh_star, sys)

    T = sys.x_star.args[-1]
    if np.allclose(sys.x_star(0,1), sys.x_star(T,1)):
        sys.E = construct_periodic_basis(sys)
    else:
        sys.E = construct_basis(sys)

    __verify_sys(sys)
    return sys


def NonlinSystem_v3(f, Jf, g, Jg, g_perp, x_star, uf, Juf, P):
    R'''
        dx/dt = f(x) + g(x) u

        :param Jf: = ∂f/∂x
        :param Juf: = ∂uf/∂x
        :param Jg: = ∂g/∂x
        :param x_star, u_star: particular solution dx⋆(t)/dt = f(x⋆(t)) + g(uf(x⋆(t))) uf(x⋆(t))
        :param g_perp: g⊥ ⋅ g = I
        :param P: projection matrix
        :return: Nonlinear System class object 
    '''

    def h(s, x):
        return s.f(x) + s.g(x).dot(s.uf(x))

    def h_star(s, t):
        x = s.x_star(t)
        return s.h(x)

    def Jh(s, x):
        Jf = s.Jf(x)
        g = s.g(x)
        Jg = s.Jg(x)
        u = s.uf(x)
        Ju = s.Juf(x)
        return Jf + np.tensordot(Jg, np.reshape(u, (-1,)), axes=(1,0)) + g.dot(Ju)

    def Jh_star(s, t):
        x = s.x_star(t)
        return s.Jh(x)

    sys = System()
    sys.f = f
    sys.Jf = Jf
    sys.g = g
    sys.Jg = Jg
    sys.g_perp = g_perp
    sys.x_star = x_star
    sys.P = P
    sys.uf = uf
    sys.Juf = Juf
    sys.h = types.MethodType(h, sys)
    sys.Jh = types.MethodType(Jh, sys)
    sys.h_star = types.MethodType(h_star, sys)
    sys.Jh_star = types.MethodType(Jh_star, sys)

    T = sys.x_star.args[-1]
    if np.allclose(sys.x_star(0,1), sys.x_star(T,1)):
        sys.E = construct_periodic_basis(sys)
    else:
        sys.E = construct_basis(sys)

    sys.E = construct_periodic_basis(sys)
    __verify_sys(sys)
    return sys


def NonlinSystem(**kwargs):
    '''
        1. dx/dt = f(x) + g(x) u
            arguments: f, Jf, g, Jg, g_perp, x_star, u_star, P

        2. dx/dt = h(x) + g(x) v
            arguments: h, Jh, g, g_perp, x_star, P

        3. dx/dt = f(x) + g(x) uf(x) + g(x) v
            arguments: f, Jf, g, Jg, g_perp, x_star, uf, Juf, P

    '''
    if 'f' in kwargs:
        if 'uf' in kwargs:
            return NonlinSystem_v3(**kwargs)
        if 'u_star' in kwargs:
            return NonlinSystem_v1(**kwargs)
        assert False, 'Not enough arguments'

    if 'h' in kwargs:
        return NonlinSystem_v2(**kwargs)

    assert False, 'Not enough arguments'

