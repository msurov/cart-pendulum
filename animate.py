import sys
from serialize import load_dict
from integrate import closest_elem
import matplotlib.animation as animation
import matplotlib.patches as patches
import matplotlib.artist
import matplotlib.pyplot as plt
from numpy import array, append, sin, cos, concatenate, linspace


class CartPendulum:

    def __init__(self, ax, t, x, phi):
        a = 1.
        self.pendulum_len = a
        self.cart_width = a/2.
        self.cart_height = a/6.
        self.wheel_radius = a/12.
        self.ball_radius = a/15.
        self.whole_radius = a/20.

        self.body = patches.Rectangle([0,0], self.cart_width, self.cart_height, color='blue')
        self.wheel1 = patches.CirclePolygon([0,0], self.wheel_radius, color='gray', resolution=16)
        self.wheel2 = patches.CirclePolygon([0,1], self.wheel_radius, color='gray', resolution=16)
        self.pendulum = patches.Polygon([[0,0],[0,0]], closed=False, color='g', lw=3)
        self.ball = patches.CirclePolygon([0,0], self.ball_radius, color='r', resolution=16)
        self.ball2 = patches.CirclePolygon([0,0], self.whole_radius, color='g', resolution=5)

        self.t = t
        self.x = x
        self.phi = phi

        xmin = min(
            min(x + self.pendulum_len * sin(phi)) - self.wheel_radius,
            min(x) - self.cart_width/2
        )
        xmax = max(
            max(x + self.pendulum_len * sin(phi)) + self.wheel_radius,
            max(x) + self.cart_width/2
        )
        ymin = min(
            min(2*self.wheel_radius + self.cart_height/2 + self.pendulum_len * cos(phi)) - self.ball_radius,
            0
        )
        ymax = max(
            max(2*self.wheel_radius + self.cart_height/2 + self.pendulum_len * cos(phi)) + self.ball_radius,
            2*self.wheel_radius + self.cart_height/2
        )

        ax.set_xlim(xmin - (xmax-xmin)*5/100,xmax + (xmax-xmin)*5/100)
        ax.set_ylim(ymin - (ymax-ymin)*5/100,ymax + (ymax-ymin)*5/100)

        ax.add_artist(self.body)
        ax.add_artist(self.wheel1)
        ax.add_artist(self.wheel2)
        ax.add_artist(self.pendulum)
        ax.add_artist(self.ball2)
        ax.add_artist(self.ball)

        self.move(0)

    def move(self, t):
        idx,_ = closest_elem(t, self.t)
        x = self.x[idx]
        phi = self.phi[idx]

        center = [x - self.cart_width/2 + self.cart_width/2, 1.2*self.wheel_radius + self.cart_height/2.]

        self.body.xy = [center[0] - self.cart_width/2, center[1] - self.cart_height/2.]
        self.ball2.xy = center
        self.wheel1.xy = [x - self.cart_width/2.2 + self.wheel_radius, self.wheel_radius]
        self.wheel2.xy = [x + self.cart_width/2.2 - self.wheel_radius, self.wheel_radius]
        self.pendulum.set_xy([
            center,
            [center[0] + self.pendulum_len * sin(phi), center[1] + self.pendulum_len * cos(phi)]
        ])
        self.ball.xy = [center[0] + self.pendulum_len * sin(phi), center[1] + self.pendulum_len * cos(phi)]

    def get_box(self):
        return (
            -self.pendulum_len - self.ball_radius,
            self.pendulum_len + self.ball_radius + self.cart_height + self.wheel_radius,
            self.pendulum_len + self.ball_radius,
            -self.pendulum_len - self.ball_radius + self.cart_height + self.wheel_radius
        )


class Graph:
    def __init__(self, ax, t, x, y, color):
        self.x = x
        self.y = y
        self.t = t
        self.graph, = ax.plot([x[0]], [y[0]], color=color, lw=0.5)

        xmin = min(x)
        xmax = max(x)
        ymin = min(y)
        ymax = max(y)

        ax.set_xlim(xmin - (xmax - xmin)*5/100, xmax + (xmax - xmin)*5/100)
        ax.set_ylim(ymin - (ymax - ymin)*5/100, ymax + (ymax - ymin)*5/100)

    def upt(self, t):
        idx,_ = closest_elem(t, self.t)
        idx += 1
        self.graph.set_data(self.x[:idx], self.y[:idx])


def twice_repeat(arr):
    return concatenate((arr, arr + arr[-1] - arr[0]), axis=0)


def run(desire, transient=None, saveto=None, speed=1.0, fps=30):

    orig_t = desire['t']
    orig_x = desire['x']
    orig_phi = desire['phi']
    orig_dphi = desire['dphi']
    orig_dx = desire['dx']
    orig_u = desire['u']

    if transient is None:
        transient = desire

    data_t = transient['t']
    data_x = transient['x']
    data_phi = transient['phi']
    data_dphi = transient['dphi']
    data_dx = transient['dx']
    data_u = transient['u']

    fig = plt.figure()
    fig.set_size_inches(4*3, 3*3)

    ax0 = plt.subplot2grid((4, 2), (0, 0), rowspan=2, colspan=2, aspect=1)
    cart = CartPendulum(ax0, data_t, data_x, data_phi)
    time_text = ax0.text(0.1, 0.1, 'xx', bbox=dict(facecolor='gray', alpha=0.2), transform = ax0.transAxes)
    ax0.grid()
    ax0.set_title('play speed %d%%' % int(speed * 100))

    ax1 = plt.subplot2grid((4, 2), (2, 0))
    graph1 = Graph(ax1, data_t, data_phi, data_x, 'blue')
    ax1.plot(orig_phi, orig_x, 'r--', lw=2)
    ax1.set_title(R'$x(\varphi)$')
    ax1.grid()

    ax2 = plt.subplot2grid((4, 2), (2, 1))
    graph2 = Graph(ax2, data_t, data_phi, data_dphi, 'blue')
    ax2.plot(orig_phi, orig_dphi, 'r--', lw=2)
    ax2.set_title(R'$\dot{\varphi}(\varphi)$')
    ax2.grid()

    ax3 = plt.subplot2grid((4, 2), (3, 0))
    graph3 = Graph(ax3, data_t, data_phi, data_dx, 'blue')
    ax3.plot(orig_phi, orig_dx, 'r--', lw=2)
    ax3.set_title(R'$\dot{x}(\varphi)$')
    ax3.grid()

    ax4 = plt.subplot2grid((4, 2), (3, 1))
    graph4 = Graph(ax4, data_t, data_t, data_u, 'blue')
    ax4.set_title(R'$u(t)$')
    ax4.grid()

    fig.tight_layout()

    def update(i):
        t = i * speed / float(fps)
        cart.move(t)
        graph1.upt(t)
        graph2.upt(t)
        graph3.upt(t)
        graph4.upt(t)
        time_text.set_text('t = %2.2fs' % t)
        return cart.body, cart.wheel1, cart.wheel2, cart.pendulum, \
            cart.ball, cart.ball2, graph1, graph2, graph3, graph4

    interval_movie = 1. * speed / fps
    nframes = int(data_t[-1] * fps / speed)

    anim = animation.FuncAnimation(fig, update, frames=nframes, blit=False, interval=interval_movie*1e+3)

    if saveto is None:
        plt.show()
    else:
        bitrate = 200. * fps
        Writer = animation.writers['ffmpeg']
        writer = Writer(fps=fps, metadata=dict(artist='Me'), bitrate=bitrate)
        print('recording movie..')
        anim.save(saveto, writer=writer)
        print(' done')


if __name__ == '__main__':
    # run_only_anim('../fig/desire_motion_2.avi', speed=0.5, fps=30)
    # run('../fig/stabilization.avi', speed=0.5, fps=30)
    # run('../fig/desire_motion.avi', speed=0.5, fps=30)
    run(speed=0.5)
