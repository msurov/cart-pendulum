import numpy as np
from scipy.integrate import ode
from scipy.signal import argrelextrema


def nonzero_intervals(arr):
    '''
        returns nonzero intervals for a given array
        for example:
            [0,1,1,1,0,0,1,1] -> [(1,3), (6,7)]
    '''
    idx, = np.nonzero(arr)
    if len(idx) == 0:
        return []

    didx = np.diff(idx)
    gaps, = np.nonzero(didx > 1)
    gaps += 1

    pairs = []
    c = idx[0]
    for g in gaps:
        pair = (c, idx[g-1])
        pairs += [pair]
        c = idx[g]

    pair = (c, idx[-1])
    pairs += [pair]

    return pairs


def closest_elem(e, arr):
    d = np.subtract(arr, e)

    if len(np.shape(e)) > 0:
        d = np.linalg.norm(d, axis=1)
    else:
        d = np.abs(d)

    i = np.argmin(d)
    return i, d[i]


def modulo(arg, base):
    n = arg // base
    return arg - n * base


def integrate_vec(rhs, x0, tspan, npoints=None, step=None, handler=None, maxval=None, **kwargs):
    '''
    Integrate vector DE in normal form: dx/dt = f(t, x)
        with x(tspan0) = x0 on interval tspan
    handler(t, x) returns
        0  to continue or
        -1 to stop
    '''
    t0, t1 = tspan
    t_arr = np.array([t0])
    x_arr = np.array([x0])

    solver = ode(rhs)
    integrator = solver.set_integrator('dopri5', **kwargs)

    if npoints is None: 
        assert step is not None
        assert step > 0.
        npoints = int(abs(tspan[0] - tspan[1]) / float(step) + 0.5) + 1

    if handler is not None:
        integrator.set_solout(handler)

    solver.set_initial_value(x0, t0)

    for i in range(1, npoints):
        t = t0 + (t1 - t0) * i / (npoints - 1)
        t = np.clip(t, min(t0, t1), max(t0, t1))
        solver.integrate(t)

        if not solver.successful:
            raise Exception('integration error')

        if abs(t - solver.t) > 1e-16:
            if solver.t != t_arr[-1]:
                t_arr = np.append(t_arr, solver.t)
                x_arr = np.vstack((x_arr, solver.y))

            return t_arr, x_arr

        t_arr = np.append(t_arr, solver.t)
        x_arr = np.vstack((x_arr, solver.y))

        if maxval is not None and np.linalg.norm(solver.y) > maxval:
            return t_arr, x_arr

    return t_arr, x_arr


def integrate(rhs, x0, tspan, **kwargs):
    '''
    Integrates matrix or vector DE in normal form:
    dX/dt = F(t,X) where X is an element of R^{NxM}
    '''
    shape = np.shape(x0)
    d = len(shape)

    if d <= 1:
        t, x = integrate_vec(rhs, x0, tspan, **kwargs)
    elif d == 2:
        ny, nx = shape
        def f(t, X): return np.reshape(rhs(t, np.reshape(X, (ny, nx))), ny * nx)
        t, x = integrate_vec(f, np.reshape(x0, ny * nx), tspan, **kwargs)
        x = np.array([np.reshape(e, (ny, nx)) for e in x])
    else:
        raise Exception('invalid dimension of x0')

    return t, x


def integrate_periodic(rhs, x0, step=1e-3, eps=None, Tmax=None, handler=None, xmax=None, **kwargs):
    t = 0
    x = np.array(x0)
    dx0 = np.array(rhs(t, x))

    if eps is None:
        eps = vect_norm(dx0) * step * 2
    if Tmax is None:
        Tmax = 1e+6 / step
    if xmax is None:
        xmax = 1e+16

    solver = ode(rhs)
    integrator = solver.set_integrator('dopri5', **kwargs)
    solver.set_initial_value(x, t)

    if handler is not None:
        integrator.set_solout(handler)

    t_arr = [t]
    x_arr = [x]

    while True:
        t = t_arr[-1] + step
        solver.integrate(t)

        if not solver.successful:
            raise Exception('integration error')

        t = solver.t
        x = np.array(solver.y)
        if t > Tmax:
            return None

        if vect_norm(x) > xmax:
            return None

        x1 = np.array(x)
        x2 = x_arr[-1]

        if dx0.dot(x1-x0) >= 0 and dx0.dot(x2-x0) < 0 and vect_norm(x1-x0) < eps:
            tau = vect_norm(x0 - x2) / vect_norm(x1 - x2) * (t - t_arr[-1])
            t = t_arr[-1] + tau
            x = x0
            t_arr.append(t)
            x_arr.append(x)
            break

        t_arr.append(t)
        x_arr.append(x)

    t_arr = np.array(t_arr)
    x_arr = np.array(x_arr)
    return t_arr, x_arr


def integrate_fixedstep(rhs, x0, tspan, step=1e-3):
    xshape = tuple(x0.shape)
    t = np.arange(tspan[0], tspan[1], step)
    x = np.zeros((len(t),) + xshape)
    x[0] = x0

    for i in range(1, len(t)):
        dx = rhs(t[i-1], x[i-1])
        if dx is None:
            t = t[:i]
            x = x[:i]
            break
        dx = np.reshape(dx, xshape)
        x[i] = x[i-1] + dx * (t[i] - t[i-1])

    return t, x


def ss_ident(size):
    assert size % 2 == 0, 'Incorrect size defined'
    S = np.zeros((size, size))
    size1 = size // 2
    S[:size1,size1:] = np.eye(size1)
    S[size1:,:size1] = -np.eye(size1)
    return S


def get_periodic_interval(arr, tol=1e-3):
    x0 = arr[0,:]
    d = np.array([vect_norm(x - x0) for x in arr])
    locmins = argrelextrema(d, np.less)[0]
    locmins = filter(lambda e: d[e] < tol, locmins)
    if len(locmins) == 0:
        raise Exception('Trajectory does not look like a periodic; try to change tolerance.')

    return 0, locmins[0] + 1


def get_max_inc_interval(x):

    seq_start = 0
    seq_len = 0
    seq_max_len = 0

    for i in range(1,len(x)):
        if x[i] < x[i-1]:
            seq_len = i - seq_start
            if seq_len > seq_max_len:
                seq_max_len = seq_len
                seq_max_start = seq_start

            seq_start = i

    seq_len = len(x) - seq_start
    if seq_len > seq_max_len:
        seq_max_len = seq_len
        seq_max_start = seq_start

    return seq_max_start, seq_max_len


def increasing_interpolate(x, y):
    interval_start, interval_len = get_max_inc_interval(x)
    interval_end = interval_start + interval_len
    _x = x[interval_start:interval_end]
    _y = y[interval_start:interval_end]
    return spline_interp(_x, _y)

